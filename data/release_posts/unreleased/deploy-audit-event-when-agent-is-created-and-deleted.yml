---
features:
  secondary:
  - name: "Audit event when agent for Kubernetes is created and deleted"
    available_in: [premium, ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/compliance/audit_event_types.html#deployment-management'
    reporter: nagyv-gitlab
    stage: deploy
    categories:
    - Audit Events
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/462749'
    description: |
      Because the agent for Kubernetes allows bi-directional data flow between a Kubernetes cluster and GitLab, it's important to know when a component that can access your systems is added or removed.
      In past releases, compliance teams had to use custom tooling or search for this data in GitLab directly. GitLab now provides the following audit events:

      - `cluster_agent_created` records who registered a new agent for Kubernetes.
      - `cluster_agent_create_failed` records who tried to register a new agent for Kubernetes but failed.
      - `cluster_agent_deleted` records who removed an agent for Kubernetes registration.
      - `cluster_agent_delete_failed` records who tried to remove an agent for Kubernetes registration but failed.

      These audit events extend the `cluster_agent_token_created` and `cluster_agent_token_revoked` audit events to further improve the ability to audit your GitLab instance.
