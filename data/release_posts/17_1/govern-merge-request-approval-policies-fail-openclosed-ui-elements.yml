---
features:
  secondary:
  - name: "Merge request approval policies fail open/closed (Policy editor)"
    available_in: [ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/application_security/policies/scan-result-policies.html#fallback_behavior'
    reporter: g.hickman
    stage: govern
    categories:
    - Security Policy Management
    epic_url: 'https://gitlab.com/groups/gitlab-org/-/epics/13227'
    image_url: '/images/17_1/fail-open.png'
    description: |
      Building on the previous [iteration](https://gitlab.com/groups/gitlab-org/-/epics/10816), we are introducing a new option within the policy editor allowing users to toggle security policies to fail open or fail closed. This enhancement extends the YAML support to allow for simpler configuration within the policy editor view.

      For example, a merge request policy configured to fail open allows a merge request to merge if there is not enough evidence to evaluate the criteria. The lack of evidence might be because an analyzer is not enabled for the project, or the analyzer failed to produce results for the policy to evaluate. This approach allows for progressive rollout of policies as teams work to ensure proper scan execution and enforcement.
