---
features:
  secondary:
  - name: "Improve automatic retry for failed CI jobs with specific exit codes"
    available_in: [core, premium, ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/ci/yaml/#retry'
    reporter: dhershkovitch
    stage: verify
    categories:
    - Pipeline Composition
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/262674'
    description: |
      Previously, you could use `retry:when` in addition to `retry:max` to configure how many times a job is retried
      when specific failures occur, like when a script fails.

      With this release, you can now use [`retry:exit_codes`](https://docs.gitlab.com/ee/ci/yaml/#retryexit_codes)
      to configure automatic retries of failed jobs based on specific script exit codes.
      You can use `retry:exit_codes` with `retry:when` and `retry:max` to fine-tune your pipeline's behavior
      according to your specific needs and improve your pipeline execution.

      Thanks to [Baptiste Lalanne](https://gitlab.com/BaptisteLalanne) for this community contribution!
