---
features:
  secondary:
  - name: "Custom webhook headers"
    available_in: [core, premium, ultimate]
    gitlab_com: true
    documentation_link: 'https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#custom-headers'
    reporter: m_frankiewicz
    stage: manage
    categories:
    - Webhooks
    issue_url: 'https://gitlab.com/gitlab-org/gitlab/-/issues/17290'
    description: |
      Previously, GitLab webhooks did not support custom headers. This meant you could not use them with systems that accept authentication tokens from headers with specific names.

      With this release, you can add up to 20 custom headers when you create or edit a webhook. You can use these custom headers for authentication to external services.

      With this feature and the [custom webhook template](https://docs.gitlab.com/ee/user/project/integrations/webhooks.html#custom-webhook-template) introduced in GitLab 16.10, you can now fully design custom webhooks. You can configure your webhooks to:

      - Post custom payloads.
      - Add any required authentication headers.

      Like secret tokens and URL variables, custom headers are reset when the target URL changes.

      Thanks to [Niklas](https://gitlab.com/Taucher2003) for [this community contribution](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/146702)!
