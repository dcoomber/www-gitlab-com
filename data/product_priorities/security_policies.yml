---
priorities:
- name: Use database read model for merge request approval policies
  url: https://gitlab.com/groups/gitlab-org/-/epics/9971
  effort: "4"
  be_dri: sashi_kumar
  fe_dri: ""
  workflow_label: in dev
  start_release: "17.0"
  target_release: "17.3"
- name: Compliance Pipeline to Security Policy Migration
  url: https://gitlab.com/groups/gitlab-org/-/epics/11275
  be_dri: Andyschoenen
  fe_dri: aturinske
  effort: "2"
  workflow_label: in dev
  start_release: "17.3"
  target_release: "17.4"
- name: Stagger or batch scheduled pipelines executed by scan execution policies
  url: https://gitlab.com/groups/gitlab-org/-/epics/13997
  be_dri: mc_rocha
  fe_dri: aturinske
  effort: "4"
  workflow_label: in dev
  start_release: "17.0"
  target_release: "17.3"
- name: Refine Policy Application Limits
  url: https://gitlab.com/groups/gitlab-org/-/epics/8084
  effort: "4"
  be_dri: bauerdominic
  fe_dri: arfedoro
  workflow_label: in dev
  start_release: "17.1"
  target_release: "17.4"
- name: Add authentication to merge request external status checks
  url: https://gitlab.com/gitlab-org/gitlab/-/issues/433035
  effort: "2"
  be_dri: "mcavoj"
  fe_dri: "arfedoro"
  workflow_label: refinement
  start_release: "17.3"
  target_release: "17.4"
- name: Scope policies by group
  url: https://gitlab.com/groups/gitlab-org/-/epics/14149
  be_dri: alan
  fe_dri: arfedoro
  effort: "3"
  workflow_label: solution validation
  start_release: "17.2"
  target_release: "17.4"
- name: Prevent branch modification when a policy disables the setting for the given group protected branches
  url: https://gitlab.com/groups/gitlab-org/-/epics/13776
  be_dri: bauerdominic
  fe_dri: aturinske
  effort: "2"
  workflow_label: planning breakdown
  start_release: "17.3"
  target_release: "17.4"
- name: Improve compatibility between security policies and security analyzers
  url: https://gitlab.com/groups/gitlab-org/-/epics/14119
  be_dri: sashi_kumar
  fe_dri: aturinske
  effort: "3"
  workflow_label: solution validation
  start_release: "17.4"
  target_release: "17.6"
- name: Override jobs with pipeline execution policy pipelines
  url: https://gitlab.com/gitlab-org/gitlab/-/issues/473189
  effort: "2"
  be_dri: "Andyschoenen"
  fe_dri: "arfedoro"
  workflow_label: in review
  start_release: "17.3"
  target_release: "17.4"
- name: Prevent jobs with empty needs from starting before `.pipeline-policy-pre` finishes
  url: https://gitlab.com/gitlab-org/gitlab/-/issues/469256
  effort: "2"
  be_dri: "mcavoj"
  fe_dri: "mcavoj"
  workflow_label: solution validation
  start_release: "17.3"
  target_release: "17.4"
- name: Add support for scheduled enforcement of custom CI in the pipeline
    execution policy type
  url: https://gitlab.com/groups/gitlab-org/-/epics/14147
  be_dri: mcavoj
  fe_dri: arfedoro
  effort: "3"
  workflow_label: planning breakdown
  start_release: "17.4"
  target_release: "17.6"
- name: Enforce scan execution in spite of "disabled GitLab CI" configuration in a
    project
  url: https://gitlab.com/groups/gitlab-org/-/epics/14057
  be_dri: mc_rocha
  fe_dri: arfedoro
  effort: "2"
  workflow_label: solution validation
  start_release: "17.5"
  target_release: "17.6"
- name: Flexible Scan Execution Policy Trigger Condition
  url: https://gitlab.com/groups/gitlab-org/-/epics/11919
  be_dri: Andyschoenen
  fe_dri: aturinske
  effort: "3"
  workflow_label: solution validation
  start_release: "17.5"
  target_release: "17.7"
- name: Support multiple distinct approval actions in merge request approval policies
  url: https://gitlab.com/groups/gitlab-org/-/epics/12319
  be_dri: bauerdominic
  fe_dri: arfedoro
  effort: "3"
  workflow_label: problem validation
  start_release: "17.5"
  target_release: "17.7"
- name: Support custom roles in merge request approval policies
  url: https://gitlab.com/groups/gitlab-org/-/epics/13550
  be_dri: sashi_kumar
  fe_dri: aturinske
  effort: "1"
  workflow_label: planning breakdown
  start_release: "17.6"
  target_release: "17.6"
- name: Exclude Components from Merge Request Approval Policies for License Approval Rules
  url: https://gitlab.com/groups/gitlab-org/-/epics/10203
  be_dri: mc_rocha
  fe_dri: arfedoro
  effort: "2"
  workflow_label: problem validation
  start_release: "17.5"
  target_release: "17.6"
- name: Push rule exceptions in Merge Request Approval Policies
  url: https://gitlab.com/groups/gitlab-org/-/epics/14090
  effort: "2"
  be_dri: "Andyshoenen"
  fe_dri: "arfedoro"
  workflow_label: problem validation
  target_release: "17.7"
  start_release: "17.6"
- name: Security Policy Impact Assessment and Audit Mode
  url: https://gitlab.com/groups/gitlab-org/-/epics/10840
  effort: "4"
  be_dri: mcavoj
  fe_dri: arfedoro
  workflow_label: solution validation
  start_release: "17.6"
  target_release: "17.9"
- name: Custom external and internal status checks approval rule in merge request approval policies
  url: https://gitlab.com/groups/gitlab-org/-/epics/10177
  be_dri: mc_rocha
  fe_dri: aturinske
  effort: "2"
  workflow_label: problem validation
  start_release: "17.7"
  target_release: "17.8"
- name: Hide the Security Policy Project and Automate Permissions
  url: https://gitlab.com/groups/gitlab-org/-/epics/5446
  be_dri: Andyschoenen
  fe_dri: arfedoro
  effort: "5"
  workflow_label: design
  start_release: "17.8"
  target_release: "18.0"
- name: Enforce push rules via security policy
  url: https://gitlab.com/groups/gitlab-org/-/epics/12101
  be_dri: sashi_kumar
  fe_dri: aturinske
  effort: "3"
  workflow_label: problem validation
  start_release: "17.8"
  target_release: "17.10"
- name: Add support for Organization Level Policies
  url: https://gitlab.com/groups/gitlab-org/-/epics/6881
  be_dri: bauerdominic
  fe_dri: arfedoro
  effort: "3"
  workflow_label: ready for design
  start_release: "17.8"
  target_release: "17.10"
- name: Approval group members are not visible to MR creator
  url: https://gitlab.com/gitlab-org/gitlab/-/issues/368487
  workflow_label: ""
  be_dri: mcavoj
  fe_dri: mcavoj
  effort: "2"
- name: Custom SAST/Secret Detection Ruleset Support for Scan Execution Policies
  url: https://gitlab.com/groups/gitlab-org/-/epics/7671
  workflow_label: design
  be_dri: ""
  fe_dri: ""
  effort: "1"
- name: Fix Security Approval Policy Notifications
  url: https://gitlab.com/groups/gitlab-org/-/epics/8314
  workflow_label: problem validation
  be_dri: ""
  fe_dri: ""
  effort: "2"
- name: Granular per-rule controls for scanners used in Merge Request Approval Policies
  url: https://gitlab.com/groups/gitlab-org/-/epics/8432
  workflow_label: problem validation
  be_dri: ""
  fe_dri: ""
  effort: "2"
- name: Provide a UI-based Policy Approval Workflow
  url: https://gitlab.com/groups/gitlab-org/-/epics/5447
  workflow_label: planning breakdown
  be_dri: ""
  fe_dri: ""
  effort: "5"
- name: Display Security Policy Errors
  url: https://gitlab.com/groups/gitlab-org/-/epics/6770
  workflow_label: problem validation
  be_dri: ""
  fe_dri: ""
  effort: "2"
