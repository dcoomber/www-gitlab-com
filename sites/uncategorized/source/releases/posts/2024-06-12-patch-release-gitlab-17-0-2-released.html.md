---
title: "GitLab Patch Release: 17.0.2, 16.11.4, 16.10.7"
categories: releases
author: Greg Myers
author_gitlab: greg
author_twitter: gitlab
description: "Learn more about GitLab Patch Release: 17.0.2, 16.11.4, 16.10.7 for GitLab Community Edition (CE) and Enterprise Edition (EE)."
canonical_path: '/releases/2024/06/12/patch-release-gitlab-17-0-2-released/'
image_title: '/images/blogimages/security-cover-new.png'
tags: security
---

<!-- For detailed instructions on how to complete this, please see https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/blog-post.md -->

Today we are releasing versions 17.0.2, 16.11.4, 16.10.7 for GitLab Community Edition (CE) and Enterprise Edition (EE).

These versions contain important bug and security fixes, and we strongly recommend that all GitLab installations be upgraded to
one of these versions immediately. GitLab.com is already running the patched version.

GitLab releases fixes for vulnerabilities in dedicated patch releases. There are two types of patch releases:
scheduled releases, and ad-hoc critical patches for high-severity vulnerabilities. Scheduled releases are released twice a month on the second and fourth Wednesdays.
For more information, you can visit our [releases handbook](https://handbook.gitlab.com/handbook/engineering/releases/) and [security FAQ](https://about.gitlab.com/security/faq/).
You can see all of GitLab release blog posts [here](/releases/categories/releases/).

For security fixes, the issues detailing each vulnerability are made public on our
[issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=closed&label_name%5B%5D=bug%3A%3Avulnerability&confidential=no&first_page_size=100)
30 days after the release in which they were patched.

We are dedicated to ensuring all aspects of GitLab that are exposed to customers or that host customer data are held to
the highest security standards. As part of maintaining good security hygiene, it is highly recommended that all customers
upgrade to the latest patch release for their supported version. You can read more
[best practices in securing your GitLab instance](/blog/2020/05/20/gitlab-instance-security-best-practices/) in our blog post.

### Recommended Action

We **strongly recommend** that all installations running a version affected by the issues described below are **upgraded to the latest version as soon as possible**.

When no specific deployment type (omnibus, source code, helm chart, etc.) of a product is mentioned, this means all types are affected.

## Security fixes

### Table of security fixes

| Title | Severity |
| ----- | -------- |
| [ReDoS in gomod dependency linker](#redos-in-gomod-dependency-linker) | Medium |
| [ReDoS in CI interpolation (fix bypass)](#redos-in-ci-interpolation-fix-bypass) | Medium |
| [ReDoS in Asana integration issue mapping when webhook is called](#redos-in-asana-integration-issue-mapping-when-webhook-is-called) | Medium |
| [XSS and content injection when viewing raw XHTML files on IOS devices](#xss-and-content-injection-when-viewing-raw-xhtml-files-on-ios-devices) | Medium |
| [Missing agentk request validation could cause KAS to panic](#missing-agentk-request-validation-could-cause-kas-to-panic) | Low |

### ReDoS in gomod dependency linker

An issue has been discovered in GitLab CE/EE affecting all versions starting from 13.1 prior to 16.10.7, starting from 16.11 prior to 16.11.4, and starting from 17.0 prior to 17.0.2. It was possible for an attacker to cause a denial of service using maliciously crafted file.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H`, 6.5).
It is now mitigated in the latest release and is assigned [CVE-2024-1495](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-1495).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.

### ReDoS in CI interpolation (fix bypass)

An issue has been discovered in GitLab CE/EE affecting all versions prior to 16.10.7, starting from 16.11 prior to 16.11.4, and starting from 17.0 prior to 17.0.2. A vulnerability in GitLab's CI/CD pipeline editor could allow for denial of service attacks through maliciously crafted configuration files.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H`, 6.5).
It is now mitigated in the latest release and is assigned [CVE-2024-1736](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-1736).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.


### ReDoS in Asana integration issue mapping when webhook is called

An issue has been discovered in GitLab CE/EE affecting all versions starting from 8.4 prior to 16.10.7, starting from 16.11 prior to 16.11.4, and starting from 17.0 prior to 17.0.2. A vulnerability in GitLab's Asana integration allowed an attacker to potentially cause a regular expression denial of service by sending specially crafted requests.
This is a medium severity issue (`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H`, 6.5).
It is now mitigated in the latest release and is assigned [CVE-2024-1963](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-1963).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.

### XSS and content injection when viewing raw XHTML files on iOS devices

A cross-site scripting issue has been discovered in GitLab affecting all versions starting from 5.1 before 16.10.7, all versions starting from 16.11 before 16.11.4, all versions starting from 17.0 before 17.0.2. When viewing an XML file in a repository in raw mode, it can be made to render as HTML if viewed under specific circumstances.
This is a high severity issue (`CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:N`, 4.4).
It is now mitigated in the latest release and is assigned [CVE-2024-4201](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-4201).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.

### Missing agentk request validation could cause KAS to panic

DoS in KAS in GitLab CE/EE affecting all versions from 16.10.0 prior to 16.10.6 and 16.11.0 prior to 16.11.3 allows an attacker to crash KAS via crafted gRPC requests.
This is a low severity issue (`CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:N/I:N/A:L`, 3.1).
It is now mitigated in the latest release and is assigned [CVE-2024-5469](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-5469).

This vulnerability has been discovered internally by the Environments team.



## Bug fixes


### 17.0.2

* [Makefile: update Git versions (v17.0 backport)](https://gitlab.com/gitlab-org/gitaly/-/merge_requests/6996)
* [Update VERSION files](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/154018)
* [Docs: Backport Dedicated AI updates](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/154019)
* [Fix failing specs in 17-0-stable-ee](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/154505)
* [Include headers in LfsDownloadObject](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/154624)
* [[17.0] Deprecate support for Ubuntu 18.04](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/155385)

### 16.11.4

* [Makefile: update Git versions (v16.11 backport)](https://gitlab.com/gitlab-org/gitaly/-/merge_requests/6997)
* [Backport 'run-release-environment-for-tag-commits' into 16.11](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/153752)
* [Dedicated AI updates](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/154041)
* [Speed up as-if-foss Rubocop](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/153833)
* [Inclusion of headers in LfsDownloadObject for GitHub imports](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/154370)
* [Fix failing specs on 16-11-stable](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/154504)
* [Stop orphaning pages deployments on Geo secondaries on 16.11](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/154838)

### 16.10.7

* [Makefile: update Git versions (v16.10 backport)](https://gitlab.com/gitlab-org/gitaly/-/merge_requests/6998)
* [Backport 'run-release-environment-for-tag-commits' into 16.10](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/153753)
* [Fix failing specs on 16-10-stable-ee](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/154503)
* [Stop orphaning pages deployments on Geo secondaries on 16.10](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/154837)

## Updating

To update GitLab, see the [Update page](/update).
To update Gitlab Runner, see the [Updating the Runner page](https://docs.gitlab.com/runner/install/linux-repository.html#updating-the-runner).

## Receive Patch Notifications

To receive patch blog notifications delivered to your inbox, visit our [contact us](https://about.gitlab.com/company/contact/) page.
To receive release notifications via RSS, subscribe to our [patch release RSS feed](https://about.gitlab.com/security-releases.xml) or our [RSS feed for all releases](https://about.gitlab.com/all-releases.xml).

## We’re combining patch and security releases

This improvement in our release process matches the industry standard and will help GitLab users get information about security and
bug fixes sooner, [read the blog post here](https://about.gitlab.com/blog/2024/03/26/were-combining-patch-and-security-releases/).
