---
title: "GitLab Patch Release: 17.2.2, 17.1.4, 17.0.6"
categories: releases
author: Costel Maxim
author_gitlab: cmaxim
author_twitter: gitlab
description: "Learn more about GitLab Patch Release: 17.2.2, 17.1.4, 17.0.6 for GitLab Community Edition (CE) and Enterprise Edition (EE)."
canonical_path: '/releases/2024/08/07/patch-release-gitlab-17-2-2-released.html.md'
image_title: '/images/blogimages/security-cover-new.png'
tags: security
---

<!-- For detailed instructions on how to complete this, please see https://gitlab.com/gitlab-org/release/docs/-/blob/master/general/patch/blog-post.md -->

Today we are releasing versions 17.2.2, 17.1.4, 17.0.6 for GitLab Community Edition (CE) and Enterprise Edition (EE).

These versions contain important bug and security fixes, and we strongly recommend that all GitLab installations be upgraded to
one of these versions immediately. GitLab.com is already running the patched version.

GitLab releases fixes for vulnerabilities in dedicated patch releases. There are two types of patch releases:
scheduled releases, and ad-hoc critical patches for high-severity vulnerabilities. Scheduled releases are released twice a month on the second and fourth Wednesdays.
For more information, you can visit our [releases handbook](https://handbook.gitlab.com/handbook/engineering/releases/) and [security FAQ](https://about.gitlab.com/security/faq/).
You can see all of GitLab release blog posts [here](/releases/categories/releases/).

For security fixes, the issues detailing each vulnerability are made public on our
[issue tracker](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=closed&label_name%5B%5D=bug%3A%3Avulnerability&confidential=no&first_page_size=100)
30 days after the release in which they were patched.

We are dedicated to ensuring all aspects of GitLab that are exposed to customers or that host customer data are held to
the highest security standards. As part of maintaining good security hygiene, it is highly recommended that all customers
upgrade to the latest patch release for their supported version. You can read more
[best practices in securing your GitLab instance](/blog/2020/05/20/gitlab-instance-security-best-practices/) in our blog post.

### Recommended Action

We **strongly recommend** that all installations running a version affected by the issues described below are **upgraded to the latest version as soon as possible**.

When no specific deployment type (omnibus, source code, helm chart, etc.) of a product is mentioned, this means all types are affected.

## Security fixes

### Table of security fixes

| Title | Severity |
| ----- | -------- |
| [Privilege Escalation via LFS Tokens Granting Unrestricted Repository Access](#privilege-escalation-via-lfs-tokens-granting-unrestricted-repository-access) | Medium |
| [Cross project access of Security policy bot](#cross-project-access-of-security-policy-bot) | Medium |
| [Advanced search ReDOS in highlight for code results](#advanced-search-redos-in-highlight-for-code-results) | Medium |
| [Denial of Service via banzai pipeline](#denial-of-service-via-banzai-pipeline) | Medium |
| [Denial of service using adoc files](#denial-of-service-using-adoc-files) | Medium |
| [ReDoS in RefMatcher when matching branch names using wildcards](#redos-in-refmatcher-when-matching-branch-names-using-wildcards) | Medium |
| [Path encoding can cause the Web interface to not render diffs correctly.](#path-encoding-can-cause-the-web-interface-to-not-render-diffs-correctly) | Medium |
| [XSS while viewing raw XHTML files through API](#xss-while-viewing-raw-xhtml-files-through-api) | Medium |
| [Ambiguous tag name exploitation](#ambiguous-tag-name-exploitation) | Medium |
| [Logs disclosings potentially sensitive data in query params](#logs-disclosings-potentially-sensitive-data-in-query-params) | Medium |
| [Password bypass on approvals using policy projects](#password-bypass-on-approvals-using-policy-projects) | Medium |
| [ReDoS when parsing git push](#redos-when-parsing-git-push) | Medium |
| [Webhook deletion audit log can preserve auth credentials](#webhook-deletion-audit-log-can-preserve-auth-credentials) | Medium |

### Privilege Escalation via LFS Tokens Granting Unrestricted Repository Access

A permission check vulnerability in GitLab CE/EE affecting all versions starting from 8.12 prior to 17.0.6, 17.1 prior to 17.1.4, and 17.2 prior to 17.2.2  allowed for LFS tokens to read and write to the user owned repositories.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:H/I:H/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:H/I:H/A:N), 6.8).
It is now mitigated in the latest release and is assigned [CVE-2024-3035](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-3035).

Thanks [pwnie](https://hackerone.com/pwnie) for reporting this vulnerability through our HackerOne bug bounty program.

### Cross project access of Security policy bot

An issue was discovered in GitLab EE affecting all versions starting from 16.0 prior to 17.0.6, starting from 17.1 prior to 17.1.4, and starting from 17.2 prior to 17.2.2, which allowed cross project access for Security policy bot.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:N `](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:N), 4.4).
It is now mitigated in the latest release and is assigned [CVE-2024-6356](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-6356).

Thanks [yvvdwf](https://hackerone.com/yvvdwf) for reporting this vulnerability through our HackerOne bug bounty program.


### Advanced search ReDOS in highlight for code results

A Denial of Service (DoS) condition has been discovered in GitLab CE/EE affecting all versions starting with 15.9 before 17.0.6, 17.1 prior to 17.1.4, and 17.2 prior to 17.2.2. It is possible for an attacker to cause catastrophic backtracking while parsing results from Elasticsearch.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:L`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:L), 4.3).
We have requested a CVE ID and will update this blog post when it is assigned.

This vulnerability was discovered internally by GitLab team member [Terri Chu](https://gitlab.com/terrichu).


### Denial of Service via banzai pipeline

Multiple Denial of Service (DoS) conditions has been discovered in GitLab CE/EE affecting all versions starting from 1.0 prior to 17.0.6, starting from 17.1 prior to 17.1.4, and starting from 17.2 prior to 17.2.2 which allowed an attacker to cause resource exhaustion via banzai pipeline.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H), 6.5).
It is now mitigated in the latest release and is assigned [CVE-2024-5423](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-5423).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.


### Denial of service using adoc files

A Denial of Service (DoS) condition has been discovered in GitLab CE/EE affecting all versions starting with 12.6 before 17.0.6, 17.1 prior to 17.1.4, and 17.2 prior to 17.2.2. It is possible for an attacker to cause a denial of service using crafted adoc files.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H), 6.5).
It is now mitigated in the latest release and is assigned [CVE-2024-4210](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-4210).

Thanks [gudanggaramfilter](https://hackerone.com/gudanggaramfilter) for reporting this vulnerability through our HackerOne bug bounty program.


### ReDoS in RefMatcher when matching branch names using wildcards

ReDoS flaw in RefMatcher when matching branch names using wildcards in GitLab EE/CE affecting all versions from 11.3 prior to 17.0.6, 17.1 prior to 17.1.4, and 17.2 prior to 17.2.2 allows denial of service via Regex backtracking.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H), 6.5).
It is now mitigated in the latest release and is assigned [CVE-2024-2800](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-2800).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.


### Path encoding can cause the Web interface to not render diffs correctly.

An issue was discovered in GitLab CE/EE affecting all versions starting from 8.16 prior to 17.0.6, starting from 17.1 prior to 17.1.4, and starting from 17.2 prior to 17.2.2, which causes the web interface to fail to render the diff correctly when the path is encoded.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:U/C:N/I:H/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:R/S:U/C:N/I:H/A:N), 5.7).
It is now mitigated in the latest release and is assigned [CVE-2024-6329](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-6329).

Thanks [st4nly0n](https://hackerone.com/st4nly0n) for reporting this vulnerability through our HackerOne bug bounty program.


### XSS while viewing raw XHTML files through API

A cross-site scripting issue has been discovered in GitLab affecting all versions starting from 5.1 prior 17.0.6, starting from 17.1 prior to 17.1.4, and starting from 17.2 prior to 17.2.2. When viewing an XML file in a repository in raw mode, it can be made to render as HTML if viewed under specific circumstances.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:N `](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:H/PR:L/UI:R/S:C/C:L/I:L/A:N), 4.4).
It is now mitigated in the latest release and is assigned [CVE-2024-4207](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-4207).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.


### Ambiguous tag name exploitation

An issue has been discovered in GitLab CE/EE affecting all versions before 17.0.6, 17.1 prior to 17.1.4, and 17.2 prior to 17.2.2. An issue was found that allows someone  to abuse a discrepancy between the Web application display and the git command line interface to social engineer victims into cloning non-trusted code.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:L/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:N/UI:N/S:U/C:N/I:L/A:N), 5.3).
It is now mitigated in the latest release and is assigned [CVE-2024-3958](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-3958).

Thanks [st4nly0n](https://hackerone.com/st4nly0n) for reporting this vulnerability through our HackerOne bug bounty program.


### Logs disclosings potentially sensitive data in query params

An issue has been discovered in GitLab CE/EE affecting all versions starting from 13.9 before 17.0.6, all versions starting from 17.1 before 17.1.4, all versions starting from 17.2 before 17.2.2. Under certain conditions, access tokens may have been logged when an API request was made in a specific manner.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:U/C:H/I:N/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:U/C:H/I:N/A:N), 4.9).
We have requested a CVE ID and will update this blog post when it is assigned.

This vulnerability was discovered internally by GitLab team member [Dominic Couture](https://gitlab.com/dcouture).


### Password bypass on approvals using policy projects

An issue was discovered in GitLab EE starting from version 16.7 before 17.0.6, version 17.1 before 17.1.4 and 17.2 before 17.2.2 that allowed bypassing the password re-entry requirement to approve a policy.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:L/I:L/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:H/PR:L/UI:N/S:U/C:L/I:L/A:N), 4.2).
It is now mitigated in the latest release and is assigned [CVE-2024-4784](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-4784).

Thanks [vexin](https://hackerone.com/vexin) for reporting this vulnerability through our HackerOne bug bounty program.


### ReDoS when parsing git push

An issue was discovered in GitLab CE/EE affecting all versions starting from 11.10 prior to 17.0.6, 17.1 prior to 17.1.4, and 17.2 prior to 17.2.2, with the processing logic for parsing invalid commits can lead to a regular expression DoS attack on the server.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:L`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:L), 4.3).
It is now mitigated in the latest release and is assigned [CVE-2024-3114](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-3114).

Thanks [joaxcar](https://hackerone.com/joaxcar) for reporting this vulnerability through our HackerOne bug bounty program.


### Webhook deletion audit log can preserve auth credentials

An issue was discovered in GitLab EE affecting all versions starting from 17.0 prior to 17.0.6, starting from 17.1 prior to 17.1.4, and starting from 17.2 prior to 17.2.2, where webhook deletion audit log preserved auth credentials.
This is a medium severity issue ([`CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:C/C:L/I:N/A:N`](https://gitlab-com.gitlab.io/gl-security/product-security/appsec/cvss-calculator/explain#explain=CVSS:3.1/AV:N/AC:L/PR:H/UI:N/S:C/C:L/I:N/A:N), 4.1).
It is now mitigated in the latest release and is assigned [CVE-2024-7586](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2024-7586).

This vulnerability was discovered internally by GitLab Team [Anton Smith](https://gitlab.com/anton).

## Bug fixes


### 17.2.2

* [Backups: Fix parsing of existing backups in Azure storage (Backport 17.2)](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1932)
* [Do not consider pool repos dangling on restore](https://gitlab.com/gitlab-org/gitaly/-/merge_requests/7147)
* [Never return nil when search for CC service](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/160253)
* [Fix issue in RTE related to adding text before a mention](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/160554)
* [Backport 'Check if params data cannot be JSONified' into 17.2](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/160666)
* [Document Rake task to show/edit token expirations](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/160434)
* [Backport 17.2 - Introduce lock-free rescheduling for duplicate job](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/160678)
* [Ignore unknown sequences in sequence fix migration](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/160767)
* [Fix squished badges rendering in 17.2](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/160744)
* [Optimize CustomAbility specs to reduce build times](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/161081)
* [Backport Do not index associated issues that are epic work item type](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/161200)
* [bug: Fix template error due to divided by zero](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/160911)
* [Put groups_direct field in CI JWT tokens behind feature flag](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/161241)
* [Backport 'Fix cluster check metrics' into 17.2](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/161430)
* [Backport Beyond Identity bug fixes to 17.2](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/161539)
* [Enable `project_daily_statistic_counter_attribute_fetch` FF by default](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/161543)
* [Backport 17.2: Release Environments - pipeline level resource group](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/161488)
* [Add require_personal_access_token_expiry application setting](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/161386)
* [Backport 17.2: Mark Cookie SameSite as default over HTTP](https://gitlab.com/gitlab-org/gitlab-pages/-/merge_requests/1030)
* [Pin QA CI tests to stable gitlab-org/gitlab branches](https://gitlab.com/gitlab-org/omnibus-gitlab/-/merge_requests/7814)

### 17.1.4

* [Backups: Fix parsing of existing backups in Azure storage (Backport 17.1)](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1933)
* [Backport 17.1 - Introduce lock-free rescheduling for duplicate job](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/160679)
* [Table driven spec needs shorter spec titles backport](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/160941)
* [Optimize CustomAbility specs to reduce build times](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/161079)
* [Put groups_direct field in CI JWT tokens behind feature flag](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/161242)
* [Increase SQL query threashold on work_items test](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/161408)
* [Backport 'Check if params data cannot be JSONified' into 17.1](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/160667)
* [Backport Beyond Identity bug fixes to 17.1](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/161541)
* [Backport gitlab-qa shm fix to 17.1 stable branch](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/161668)
* [Add require_personal_access_token_expiry application setting](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/161388)

### 17.0.6

* [Backups: Fix parsing of existing backups in Azure storage (Backport 17.0)](https://gitlab.com/gitlab-org/build/CNG/-/merge_requests/1934)
* [Backport 17.0 - Introduce lock-free rescheduling for duplicate job](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/160815)
* [Table driven spec needs shorter spec titles backport](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/160940)
* [Put groups_direct field in CI JWT tokens behind feature flag](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/161240)
* [Add require_personal_access_token_expiry application setting](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/161389)

### 16.11.8

* [Add require_personal_access_token_expiry application setting](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/161391)

### Add require_personal_access_token_expiry application setting

This default enabled, optional setting added for admins of GitLab self-managed instances on versions 16.11 and above allow them to enable mandatory expiraton on all new personal, project and group access tokens. Expirations set for existing tokens are not affected by this setting. For usage information see [Require expiration dates for new access tokens](https://docs.gitlab.com/ee/administration/settings/account_and_limit_settings.html#require-expiration-dates-for-new-access-tokens)

## Updating

To update GitLab, see the [Update page](/update).
To update Gitlab Runner, see the [Updating the Runner page](https://docs.gitlab.com/runner/install/linux-repository.html#updating-the-runner).

## Receive Patch Notifications

To receive patch blog notifications delivered to your inbox, visit our [contact us](https://about.gitlab.com/company/contact/) page.
To receive release notifications via RSS, subscribe to our [patch release RSS feed](https://about.gitlab.com/security-releases.xml) or our [RSS feed for all releases](https://about.gitlab.com/all-releases.xml).

## We’re combining patch and security releases

This improvement in our release process matches the industry standard and will help GitLab users get information about security and
bug fixes sooner, [read the blog post here](https://about.gitlab.com/blog/2024/03/26/were-combining-patch-and-security-releases/).
