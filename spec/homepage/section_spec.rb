describe Gitlab::Homepage::Section do
  subject(:section) { described_class.new(key, data) }
  let(:key) { 'core_patform' }
  let(:data) do
    {
      'name' => 'Core Platform'
    }
  end

  describe '#method_missing' do
    it 'returns value by key' do
      expect(section.name).to eq(data['name'])
    end

    context 'when key is missing' do
      it { expect(section.unknown).to be_nil }
    end
  end

  describe '#label' do
    subject { section.label }

    context 'when name is defined' do
      it { is_expected.to eq("#{described_class::LABEL_PREFIX}core patform") }
    end

    context 'when label is defined' do
      let(:label_name) { 'section::core-platform' }
      let(:data) { { 'label' => label_name } }

      it { is_expected.to eq(label_name) }
    end
  end

  describe '.all!' do
    subject { described_class.all! }

    before do
      allow(YAML).to receive(:load_file) do
        {
          'dev' => { 'name' => 'Dev' },
          'ops' => { 'name' => 'Ops' }
        }
      end
    end

    it 'returns Section objects' do
      section = subject

      expect(section.count).to eq(2)
      expect(section.map(&:name)).to match_array(%w[Dev Ops])
    end
  end
end
