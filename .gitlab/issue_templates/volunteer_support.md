## Program Overview 

Thank you for your interest in the GiveLab Volunteer Program. GitLab encourages team members to volunteer in their local communities, participate in virtual volunteer activities, and organize volunteer activities as part of team events and get togethers. Team members can also request support from the ESG Team to organize local or virtual volunteer opportunities on their behalf.

## Eligibility 

GitLab supports registered 501c3 (or country equivalent) nonprofit organizations in good standing that align with our [Values](https://about.gitlab.com/handbook/values/). A “Registered Nonprofit Organization” is one that has been registered with the local government or authorized agency within its applicable local, state, provincial, federal, or national government.

## Application Process

Please complete the below section to the best of your ability.

Please select an option (multiple options can be selected):

 - [ ] I have a volunteer activity or nonprofit in mind and would like to submit a volunteer opportunity for approval
 - [ ] I would like the ESG team’s help in organizing a volunteer activity
 - [ ] I am organizing a volunteer activity for a TMRG

1. Nonprofit organization name: `your answer here`
2. Is this nonprofit organization a 501c3 (or country equivalent) in good standing? `(yes/no/not sure)`
3. What is the mission of the nonprofit organization? `your answer here`
4. How does the nonprofit organization align with GitLab’s values and mission? `your answer here`
5. Is this request for a specific program or event? If so, please provide the details. `your answer here`
6. What is the date of the volunteer opportunity? `your answer here`
7. How many team members do you anticipate will be volunteering? `your answer here`
8. Is this an in-person or a virtual opportunity? `your answer here`
9. If this is for a group volunteer activity, do you have budget to support this activity? Often nonprofits will charge a small fee for supplies or staff time for group activities. `your answer here`

Please allow the ESG team a minimum of 10 working days to review the request. Once a decision has been made, a tag will be added as ‘approved’ or ‘declined.’


/assign @slcline, @kbuncle, @laurenlopez
/label ~ESG::Ready for Review ~volunteer-support
/epic &2145
/confidential
